#include "VulkApp.h"

#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>

int main() {
	VulkApp app;
	try {
		app.run();
	}
	catch (const std::runtime_error& e) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	return (0);
}